### What is this repository for? ###

* This is an example of cucumber and puppeteer project.

### How do I get set up? ###

* After download this repo run the command "npm i" to install the necessary node modules.

### How do I run it? ###

* To generate the default test, run the command "npm test".
* To generate the default report, run the command "node run report".
* To generate both, run the command "npm run generate".

### Credits ###

* This repo is based on the tutorial: https://chercher.tech/puppeteer/cucumber-puppeteer
